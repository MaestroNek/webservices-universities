const Sequelize = require("sequelize");


const db = new Sequelize(
	process.env.DB_NAME,
	process.env.DB_USER,
	process.env.DB_PASS,
	{
		dialect: "postgres",
		logging: false,
		define: {
			underscored: true
		}
	}
);

module.exports = db;