const getPerson = require('../utils/getPerson.js');
const { Faculty, University } = require('../models/index.js');
const response = require('../utils/response');
const error = require('../utils/error');


module.exports = {
	getFaculty(req, res){
		Faculty
			.findOne({where: {id: req.params.facultyId}})
			.then(faculty => {
				if (!faculty){
					throw "Факультет не найден"
				} else {
					response(req, res, {faculty});
				}
			})
			.catch(error(req, res))
	},

	addFaculty(req, res){
		Faculty
			.findOne({where: {name: req.body.name}})
			.then(result => {
				if (result) {
					throw 'Факультет уже создан.'
				} else {
					if (!/^([А-я]|[A-z]|\s){1,}$/i.test(req.body.name)){
						throw 'Недопустимое название факультета';
					} else {
						const {dean} = req.body;
						return getPerson(dean);
					}
				}
			})
			.then(deanId => {
				return University
					.findOne({where: {id: req.params.universityId}})
					.then(university => ({deanId, university}))
			})
			.then(({deanId, university}) => {
				if (!university) {
					throw "Университет с указанным id не найден";
				} else {
					return Faculty
						.create({
							universityId: req.params.universityId,
							deanId,
							name: req.body.name,
						});
				}
			})
			.then(faculty => {
				response(req, res, {faculty});
			})
			.catch(error(req, res))
	},

	updateFaculty(req, res){
		Faculty
			.findOne({where: {id: req.params.facultyId}})
			.then(faculty => {
				if (!faculty){
					throw "Факультет не найден";
				} else {
					if (req.body.name && !/^([А-я]|[A-z]|\s){1,}$/i.test(req.body.name)){
						throw 'Недопустимое название факультета';
					} else {
						if (req.body.dean){
							return getPerson(req.body.dean)
								.then(deanId => {
									return {deanId, faculty}
								})
						} else {
							return {deanId: null, faculty}
						}
					}
				}
			})
			.then(({deanId, faculty}) => {
				if (req.body.universityId){
					return University
						.findOne({where: {id: req.body.universityId}})
						.then(university => {
							if (!university) {
								throw "Университет не найден";
							} else {
								return faculty
									.update({
										universityId: req.body.universityId,
										name: req.body.name ? req.body.name : faculty.name,
										deanId: deanId ? deanId : faculty.deanId,
									})
							}
						})
				} else {
					return faculty
						.update({
							name: req.body.name ? req.body.name : faculty.name,
							deanId: deanId ? deanId : faculty.deanId,
						})
				}
			})
			.then(faculty => {
				response(req, res, {faculty});
			})
			.catch(error(req, res))
	},

	deleteFaculty(req, res){
		Faculty
			.findOne({where: {id: req.params.facultyId}})
			.then(faculty => {
				if (!faculty) {
					throw "Факультет не найден"
				} else {
					faculty.destroy();
					response(req, res, {status: 1});
				}
			})
			.catch(error(req, res))
	}
};