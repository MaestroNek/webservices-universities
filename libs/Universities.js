const sequelize = require('sequelize');
const Op = sequelize.Op;
const response = require('../utils/response');
const error = require('../utils/error');

const getPerson = require('../utils/getPerson.js');
const { University } = require('../models/index.js');

module.exports = {
	getUniversities(req, res){
		University
			.findAll()
			.then(universities => {
				response(req, res, {universities});
			})
			.catch(error(req, res))
	},

	addUniversity(req, res){
		University
			.findOne({
				where: {name: req.body.name},
			})
			.then(result => {
				if (result) {
					throw 'Университет уже создан.'
				} else {
					if (!/^([А-я]|[A-z]|\s){1,}$/i.test(req.body.name)){
						throw 'Недопустимое название университета';
					} else {
						const {rector} = req.body;
						return getPerson(rector);
					}
				}
			})
			.then((rectorId = null) => {
				return University
					.create({
						rectorId,
						name: req.body.name,
					})
			})
			.then(university => {
				response(req, res, {university});
			})
			.catch(error(req, res))
	},

	deleteUniversity(req, res){
		University
			.findOne({where: {id: req.params.universityId}})
			.then(university => {
				if (!university){
					throw "Университет не найден"
				} else {
					university.destroy();
					response(req, res, {status: 1});
				}
			})
			.catch(error(req, res))
	},

	updateUniversity(req, res){
		University
			.findOne({where: {id: req.params.universityId}})
			.then(university => {
				if (!university){
					throw "Университет не найден"
				} else {
					if (req.body.name && !/^([А-я]|[A-z]|\s){1,}$/i.test(req.body.name)){
						throw 'Недопустимое название университета';
					} else {
						if (req.body.rector){
							return getPerson(req.body.rector)
								.then(rectorId => {
									return university
										.update({
											name: req.body.name ? req.body.name : university.name,
											rectorId: rectorId,
										});
								})
						} else {
							return university
								.update({
									name: req.body.name ? req.body.name : university.name
								});
						}
					}
				}
			})
			.then(university => {
				response(req, res, {university});
			})
			.catch(error(req, res))
	},

	findUniversity(req, res){
		University
			.findAll({where: {name: {[Op.iLike]: `%${req.body.name}%`}}})
			.then(result => {
				response(req, res, {result});
			})
			.catch(error(req, res))
	}
};