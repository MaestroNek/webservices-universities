const sequelize = require('sequelize');
const Op = sequelize.Op;
const response = require('../utils/response');
const error = require('../utils/error');

const { Group, Faculty } = require('../models/index.js');


module.exports = {
	getGroup(req, res){
		Group
			.findOne({where: {id: req.params.groupId}})
			.then(group => {
				if (!group){
					throw "Учебная группа не найдена"
				} else {
					response(req, res, {group});
				}
			})
			.catch(error(req, res))
	},

	addGroup(req, res){
		Faculty
			.findOne({where: {id: req.params.facultyId}})
			.then(result => {
				if (!result){
					throw "Факультет не найден"
				} else {
					return Group
						.findOne({where: {number: req.body.number, facultyId: req.params.facultyId}})
				}
			})
			.then(result => {
				if (result) {
					throw "Учебная группа с таким номером уже существует на указанном факультете"
				} else {
					return Group
						.create({
							number: req.body.number,
							facultyId: req.params.facultyId
						})
				}
			})
			.then(group => {
				response(req, res, {group});
			})
			.catch(error(req, res))
	},

	updateGroup(req, res){
		Group
			.findOne({where: {id: req.params.groupId}})
			.then(group => {
				if (!group) {
					throw "Учебная группа не найдена"
				} else {
					if (req.body.facultyId) {
						return Faculty
							.findOne({where: {id: req.body.facultyId}})
							.then(result => {
								if (!result) {
									throw "Факультет не найден"
								} else {
									return {facultyId: req.body.facultyId, group}
								}
							})
					} else {
						return {facultyId: group.facultyId, group}
					}
				}
			})
			.then(({facultyId, group}) => {
				return group
					.update({
						facultyId,
						number: req.body.number,
					})
			})
			.then(group => {
				response(req, res, {group});
			})
			.catch(error(req, res))
	},

	deleteGroup(req, res){
		Group
			.findOne({where: {id: req.params.groupId}})
			.then(group => {
				if (!group){
					throw "Учебная группа не найдена"
				} else {
					group.destroy();
					//TODO: Добавить удаление студентов
					response(req, res, {status: 1});
				}
			})
			.catch(error(req, res))
	},

	findGroup(req, res){
		Group
			.findAll({where: {number: {[Op.iLike]: `%${req.body.number}%`}}})
			.then(result => {
				response(req, res, {result});
			})
			.catch(error(req, res))
	}
};