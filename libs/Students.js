const { Student, Group, Faculty } = require('../models/index.js');
const getPerson = require('../utils/getPerson.js');
const response = require('../utils/response');
const error = require('../utils/error');

module.exports = {
	getStudent(req, res){
		Student
			.findOne({where: {id: req.params.studentId}})
			.then(student => {
				if (!student){
					throw "Студент не найден";
				} else {
					response(req, res, {student});
				}
			})
			.catch(error(req, res));
	},

	addStudent(req, res){
		getPerson(req.body)
			.then(personId => {
				return Student
					.findOne({where: {userId: personId}})
					.then(student => ({student, personId}));
			})
			.then(({student, personId}) => {
				if (student) {
					throw "Данный студент уже существует";
				} else {
					return Student
						.create({
							userId: personId,
							groupId: req.params.groupId,
						})
				}
			})
			.then(student => {
				response(req, res, {student});
			})
			.catch(error(req, res));
	},

	updateStudent(req, res){
		Student
			.findOne({where: {id: req.params.studentId}})
			.then(student => {
				if (!student){
					throw "Студент не найден"
				} else {
					return Group
						.findOne({where: {id: req.body.groupId}})
						.then(group => {
							if (!group){
								throw "Группа не найдена"
							} else {
								return student
									.update({
										groupId: req.body.groupId,
									})
									.then(student => {
										response(req, res, {student});
									})
									.catch(error(req, res));
							}
						});
				}
			})
			.catch(error(req, res));
	},

	deleteStudent(req, res){
		Student
			.findOne({where: {id: req.params.studentId}})
			.then(student => {
				if (!student){
					throw "Студент не найден"
				} else {
					return student
						.destroy();
				}
			})
			.then(student => {
				response(req, res, {status: 1});
			})
			.catch(error(req, res));
	},
};