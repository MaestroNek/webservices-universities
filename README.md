<h1>API Университетов по предмету "Проектирование Веб-сервисов"</h1>

Все POST/PUT запросы принимают body в формате JSON и доступны по адресу http://\*имя-сервера\*/api/

Документация по API людей, по которой необходимо придерживаться формата данных, при указании людей (ректоры, деканы и пр.), доступны  на http://persons.std-247.ist.mospolytech.ru/api/ или https://github.com/Romjkez/people-service/blob/master/README.md#%D1%81%D0%B5%D1%80%D0%B2%D0%B8%D1%81-%D0%BB%D1%8E%D0%B4%D0%B8

<h4>Университеты</h4>

---

**` POST /universities/` - создать университет**
<br>
Поля: 
* `name: String` - название университета
* `rector: Object`: - ректор, см. API людей 
  	

В случае успеха: возвращает `university: Object` c полями `id, rectorId, name, updatedAt, createdAt`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>

**` GET /universities/` - получить список университетов**
<br>
  	

В случае успеха: возвращает `universities: Array` c объектами-университетами, каждый из которых имеет следующие поля: `id, rectorId, name, updatedAt, createdAt`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>

**` DELETE /universities/:university-id` - удалить университет**
<br>
  	

В случае успеха: возвращает `status: Integer` со значением `1`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>

**` PUT /universities/:university-id` - редактировать университет**
<br>
Поля: 
* `name: String` - название университета
* `rector: Object`: - ректор,  см. API людей
  	

В случае успеха: возвращает `university: Object` c полями `id, rectorId, name, updatedAt, createdAt`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>

**` POST /universities/find` - поиск университетов**
<br>
Поля: 
* `name: String` - часть названия университета, по которой будет осуществляться поиск (не чувствительно к регистру)
  	

В случае успеха: возвращает `result: Array` c объектами-университетами, каждый из которых имеет следующие поля: `id, rectorId, name, updatedAt, createdAt`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>

<h4>Факультеты</h4>

---

**` POST /faculties/:university-id` - создать факультет**
<br>
Поля: 
* `name: String` - название факультета
* `dean: Object`: - декан, см. API людей
  	

В случае успеха: возвращает `faculty: Object` c полями `id, universityId, deanId, name, updatedAt, createdAt`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>

**` GET /faculties/:faculty-id` - получить информацию о факультете**
<br>
  	
В случае успеха: возвращает `faculty: Object` c полями `id, universityId, deanId, name, updatedAt, createdAt`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>


**` DELETE /faculties/:faculty-id` - удалить факультет**
<br>

В случае успеха: возвращает `status: Integer` со значением `1`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>
  	
**` PUT /faculties/:faculty-id` - редактировать факультет**
<br>
Поля: 
* `name: String` - название факультета
* `dean: Object`: - декан, см. API людей
  	

В случае успеха: возвращает `faculty: Object` c полями `id, universityId, deanId, name, updatedAt, createdAt`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>
  	
<h4>Группы</h4>

---

**` POST /groups/:faculty-id` - создать группу**
<br>
Поля: 
* `number: String` - номер группы
  	

В случае успеха: возвращает `group: Object` c полями `id, facultyId, number, updatedAt, createdAt`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>

**` GET /groups/:group-id` - получить информацию о группе**
<br>
  	

В случае успеха: возвращает `group: Object` c полями `id, facultyId, number, updatedAt, createdAt`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>

**` DELETE /groups/:group-id` - удалить группу**
<br>
  	

В случае успеха: возвращает `status: Integer` со значением `1`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>
  
**` PUT /groups/:group-id` - редактировать группу**
<br>
Поля: 
* `number: String` - номер группы
  	

В случае успеха: возвращает `group: Object` c полями `id, facultyId, number, updatedAt, createdAt`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>

**` POST /groups/find` - поиск групп**
<br>
Поля: 
* `number: String` - часть номера группы, по которой будет осуществляться поиск (не чувствительно к регистру)
  	

В случае успеха: возвращает `result: Array` c объектами-группами, каждый из которых имеет следующие поля: `id, facultyId, number, updatedAt, createdAt`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>

<h4>Студенты</h4>

---

**` POST /students/:group-id` - создать студента**
<br>
Поля: 
* см. API людей 
  	

В случае успеха: возвращает `student: Object` c полями `id, userId, groupId, updatedAt, createdAt`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>

**` GET /students/:student-id` - получить информацию о студенте**
<br>


В случае успеха: возвращает `student: Object` c полями `id, userId, groupId, updatedAt, createdAt`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>

**` DELETE /students/:student-id` - удалить студента**
<br>
  	

В случае успеха: возвращает `status: Integer` со значением `1`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>
	
**` PUT /students/:student-id` - редактировать студента**
<br>
Поля: 
* `groupId: Integer` - id группы
  	

В случае успеха: возвращает `student: Object` c полями `id, userId, groupId, updatedAt, createdAt`
<br>
В случае ошибки: возвращает поле `error: String` с описанием ошибки

<br/>