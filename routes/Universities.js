const express = require('express');
const router = express.Router();
const {
	getUniversities,
	addUniversity,
	deleteUniversity,
	updateUniversity,
	findUniversity,
} = require('../libs/Universities.js');


router.post('/find/', (req, res) => {
	findUniversity(req, res);
});

router.get('/', (req, res) => {
	getUniversities(req,res);
});

router.post('/', (req, res) => {
	addUniversity(req,res);
});

router.delete('/:universityId', (req, res) => {
	deleteUniversity(req,res);
});

router.put('/:universityId', (req, res) => {
	updateUniversity(req,res);
});


module.exports = router;