const express = require('express');
const router = express.Router();
const {
	getFaculty,
	addFaculty,
	updateFaculty,
	deleteFaculty,
} = require('../libs/Faculties.js');

router.get('/:facultyId', (req, res) => {
	getFaculty(req,res);
});

router.post('/:universityId', (req, res) => {
	addFaculty(req,res);
});

router.put('/:facultyId', (req, res) => {
	updateFaculty(req,res);
});

router.delete('/:facultyId', (req, res) => {
	deleteFaculty(req,res);
});


module.exports = router;