const universities = require('./Universities.js');
const faculties = require('./Faculties.js');
const groups = require('./Groups.js');
const students = require('./Students.js');


module.exports = {
	universities,
	faculties,
	groups,
	students,
};