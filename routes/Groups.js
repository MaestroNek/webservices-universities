const express = require('express');
const router = express.Router();
const {
	getGroup,
	addGroup,
	updateGroup,
	deleteGroup,
	findGroup,
} = require('../libs/Groups.js');


router.post('/find/', (req, res) => {
	findGroup(req,res);
});

router.get('/:groupId', (req, res) => {
	getGroup(req,res);
});

router.post('/:facultyId', (req, res) => {
	addGroup(req,res);
});

router.put('/:groupId', (req, res) => {
	updateGroup(req,res);
});

router.delete('/:groupId', (req, res) => {
	deleteGroup(req,res);
});


module.exports = router;