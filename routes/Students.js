const express = require('express');
const router = express.Router();
const {
	addStudent,
	getStudent,
	deleteStudent,
	updateStudent,
} = require('../libs/Students.js');


router.get('/:studentId', (req, res) => {
	getStudent(req, res);
});

router.post('/:groupId', (req, res) => {
	addStudent(req, res);
});

router.put('/:studentId', (req, res) => {
	updateStudent(req, res);
});

router.delete('/:studentId', (req, res) => {
	deleteStudent(req, res);
});

module.exports = router;