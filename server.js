require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const requestIp = require('request-ip');
const db = require('./db.js');
const app = express();
const port = process.env.PORT || 5000;

const {
	universities,
	faculties,
	groups,
	students,
} = require("./routes/index");
app.use(bodyParser.json());
app.use(requestIp.mw());
app.use('/api/universities/', universities);
app.use('/api/faculties/', faculties);
app.use('/api/groups/', groups);
app.use('/api/students/', students);

db.sync()
	.then(() => {
		console.log(`Database connected`);
		app.listen(port, () => console.log(`Server started on port ${port}`));
	})
	.catch(err => console.log(err));
