const { Log } = require('../models/index.js');

module.exports = (req, data, status) => {
	return Log
		.create({
			reqBody: req.body,
			reqParams: req.params,
			status: status,
			resBody: data,
			date: new Date().toString(),
			ip: req.clientIp,
		});
};