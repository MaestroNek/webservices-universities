const createLog = require("./log");

module.exports = (req, res, data) => {
	createLog(req, data, 200)
		.then(() => {
			res.json(data);
		});
};