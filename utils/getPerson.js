const axios = require('axios');
const generateParams = require('./generateParams.js');


function createPerson(personData) {
	return axios
		.post(`${process.env.PEOPLE_API_URL}/person`, personData)
		.then(res => {
			console.log(res);
			return res.data.data.id;
		})
		.catch(err => {
			throw err;
		});
}

function getPerson(personData, createNew = true){
	return new Promise((resolve, reject) => axios
		.get(`${process.env.PEOPLE_API_URL}/person${generateParams(personData)}`)
		.then(res => {
			resolve(res.data.id)
		})
		.catch(err => {
			if (createNew){
				console.log("СОЗДАЮ НОВОГО ЮЗЕРА");
				resolve(createPerson(personData));
			} else {
				resolve(null);
			}
			console.log(err);
			reject("Возникла ошибка при подключении к базе данных пользователей");
		}))
		.catch(err => {
			throw err;
		});
}

module.exports = getPerson;