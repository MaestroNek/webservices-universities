const createLog = require("./log");

module.exports = (req, res) => err => {
	createLog(req, {error: err}, 500)
		.then(() => {
			console.log(err);
			res.status(500).json({
				error: err,
			})
		})
};