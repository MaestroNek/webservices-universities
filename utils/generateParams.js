module.exports = (object) => {
	return ["?", Object.entries(object).map(([key, value]) => encodeURI(`${key}=${value}&`)).join('')].join('').slice(0, -1);
};