const Sequelize = require('sequelize');
const {Model} = Sequelize;
const db = require('../db.js');

class Faculty extends Model {}

Faculty.init({
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false,
	},
	name: {
		type: Sequelize.STRING,
		allowNull: false,
	},
	universityId: {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	deanId: {
		type: Sequelize.INTEGER,
		allowNull: false,
	}

}, {sequelize: db, modelName: "faculty"});

module.exports = Faculty;