const Sequelize = require('sequelize');
const {Model} = Sequelize;
const db = require('../db.js');

class Log extends Model {}

Log.init({
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false,
	},
	reqBody: {
		type: Sequelize.JSON,
	},
	reqParams: {
		type: Sequelize.JSON,
	},
	status: {
		type: Sequelize.INTEGER,
	},
	resBody: {
		type: Sequelize.JSON,
	},
	date: {
		type: Sequelize.STRING,
	},
	ip: {
		type: Sequelize.STRING,
		allowNull: false,
	}

}, {sequelize: db, modelName: "logs"});

module.exports = Log;