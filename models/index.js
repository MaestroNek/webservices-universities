const Faculty = require('./Faculty.js');
const Group = require('./Group.js');
const Student = require('./Student.js');
const University = require('./University.js');
const Log = require('./Log.js');

module.exports = {
	Faculty,
	Group,
	Student,
	University,
	Log,
};