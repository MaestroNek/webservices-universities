const Sequelize = require('sequelize');
const {Model} = Sequelize;
const db = require('../db.js');

class Group extends Model {}

Group.init({
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false,
	},
	number: {
		type: Sequelize.STRING,
		allowNull: false,
	},
	facultyId: {
		type: Sequelize.INTEGER,
		allowNull: false,
	}
}, {sequelize: db, modelName: "group"});

module.exports = Group;