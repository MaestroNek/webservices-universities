const Sequelize = require('sequelize');
const {Model} = Sequelize;
const db = require('../db.js');

class University extends Model {}

University.init({
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false,
	},
	name: {
		type: Sequelize.STRING,
		unique: true,
		allowNull: false,
	},
	rectorId: {
		type: Sequelize.INTEGER,
		allowNull: true,
	}
}, {sequelize: db, modelName: "university"});

module.exports = University;