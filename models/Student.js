const Sequelize = require('sequelize');
const {Model} = Sequelize;
const db = require('../db.js');

class Student extends Model {}

Student.init({
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false,
	},
	userId: {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	groupId: {
		type: Sequelize.INTEGER,
		allowNull: false,
	}
}, {sequelize: db, modelName: "student"});

module.exports = Student;